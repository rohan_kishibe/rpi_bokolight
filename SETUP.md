イメージ書き込み

# diskutil list
# df -h

# diskutil umount /dev/disk2s1
# df -h

# dd bs=1m if=/Users/Shared/2017-04-10-raspbian-jessie-lite.img of=/dev/disk2

1768+1 records in
1768+1 records out
1854418944 bytes transferred in 3431.090954 secs (540475 bytes/sec)

login
pi/raspberry


raspi-config

  Change User Password
    ameno431

  Advanced Options > Hostname
    bokolight

  Localisation Option > Change Locale
    no change

  Localisation Option > Change Timezone
    Asia/Tokyo

  Localisation Option > ChangeKeyboard Layout
    Generic 105-key (Intl) PC
    Other - Japanese - Japanese OADG 109A

  Localisation Option > WiFi Country
    JA

  Interfacing Options
    SSH on



/etc/network/interface

allow-hotplug wlan0
iface wlan0 inet dhcp


wpa_cli scan

wpa_cli wps_pbc

sh -c 'wpa_passphrase iPhoneT Fixz54385438 >> /etc/wpa_supplicant/wpa_supplicant.conf'



apt-get update

apt-get install oracle-java8-jdk
apt-get install tomcat7



FWsetting




WiFiCountry
日本語フォントインストール
キーボード
タイムゾーン
NTP
キーボード
パスワード
ネットワーク
ホスト名 bokolight.local

アップデート
apt-get update
apt-get -y dist-upgrade
(apt-get -y autoremove)
(apt-get -y autoclean)

unattended-upgrades


必要パッケージ追加
apt-get install oracle-java8-jdk
apt-get install tomcat7

apt-get install git
apt-get install scons
apt-get install swig

apt-get install python-dev

apt-get upgrade

rpi_ws281x-master
git clone https://github.com/jgarff/rpi_ws281x.git

cd rpi_ws281x
scons
cd python
sudo python setup.py install

git clone https://bitbucket.org/rohan_kishibe/rpi_bokolight.git

systemctl enable tomcat7

vi /etc/tomcat7/server.xml

    <Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               URIEncoding="UTF-8"
               redirectPort="8443" 
               proxyPort="80" />

apt-get install iptables

# iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8080


vi /etc/iptables/rules.v4

*nat
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
-I PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080


/etc/rc.local

# iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8080


# if gpio17=1 then run wps
gpio=17
if [ ! -d /sys/class/gpio/gpio${gpio} ]
then
  echo $gpio > /sys/class/gpio/export
fi
value=/sys/class/gpio/gpio${gpio}/value
current_value=`cat $value`
if [ $current_value -eq 1 ]
then
  ifdown wlan0
  cp -p /srv/default/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
  ifup wlan0
  sleep 5
  echo "Starting WPS Push Button Connection..."
  `/sbin/wpa_cli wps_pbc`
  `/sbin/wpa_cli wps_pin any 12345678`
fi
echo $gpio > /sys/class/gpio/unexport



vi /boot/config.txt
---
hdmi_safe=1
↓
#hdmi_safe=1
---



Pin#1 3.3V - - - Pin#11 GPIO17 WPS Setting pin

Pin#2 5V ----- (LED + red)
Pin#6 GND ----- (LED - black)
Pin#12 GPIO18 ----- (LED s green)

Pin#15 GPIO22 ----- Pin#25 GND